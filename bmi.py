# CHALLENGE
# Can you use other Python Operators and if statements to write a function that calculates BMI (bmi = weight / height^2)

def cal_bmi(weight, height):
    bmi = weight / ((height / 100) ** 2)
    return round(bmi,2)

weight = float(input("Enter the weight: "))
height = float(input("Enter the height: "))
bmi    = cal_bmi(weight, height)

print(f"Your BMI is: {bmi}")

# if bmi <= 18.5 return "Underweight"
if bmi <= 18.5:
    print("You're Underweight")

# if bmi <= 25.0 return "Normal"
elif bmi <= 25.0:
    print("Your weight is Normal")

# if bmi <= 30.0 return "Overweight"
elif bmi <= 30.0:
    print("You're Overweight")

# if bmi > 30 return "Obese"
else:
    print("You're Obese")